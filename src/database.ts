export interface DBElement {
  id: number;
  cover: string;
  name: string;
  published: {
    start: number
    end: number,
  };
}

export const db = new Map<number, DBElement>([[
  1, {
  id: 1,
  cover: 'https://live.staticflickr.com/65535/47963688361_66087679d4_o_d.jpg',
  name: 'Брунгильда во тьме',
  published: {
    start: 1551913968,
    end: 0,
  },
} ], [ 2, {
  id: 2,
  cover: 'https://live.staticflickr.com/65535/47963701778_c25311aa36_o_d.jpg',
  name: 'Готовка в ином мире',
  published: {
    start: 1551913968,
    end: 0,
  },
} ], [ 3, {
  id: 3,
  cover: 'https://mfiles.alphacoders.com/802/802452.png',
  name: 'Untitled',
  published: {
    start: 1551913968,
    end: 0,
  },
}], [ 4, {
  id: 4,
  cover: 'https://mfiles.alphacoders.com/802/802448.jpg',
  name: 'Untitled',
  published: {
    start: 1551913968,
    end: 0,
  },
}], [ 5, {
  id: 5,
  cover: 'https://mfiles.alphacoders.com/802/802451.jpg',
  name: 'Untitled',
  published: {
    start: 1551913968,
    end: 0,
  },
}], [ 6, {
  id: 6,
  cover: 'https://mfiles.alphacoders.com/802/802461.jpg',
  name: 'Untitled',
  published: {
    start: 1551913968,
    end: 0,
  },
}], [ 7, {
  id: 7,
  cover: 'https://mfiles.alphacoders.com/801/801956.png',
  name: 'Untitled',
  published: {
    start: 1551913968,
    end: 0,
  },
}], [ 8, {
  id: 8,
  cover: 'https://mfiles.alphacoders.com/800/800301.jpg',
  name: 'Untitled',
  published: {
    start: 1551913968,
    end: 0,
  },
}], [ 9, {
  id: 9,
  cover: 'https://mfiles.alphacoders.com/799/799529.jpg',
  name: 'Untitled',
  published: {
    start: 1551913968,
    end: 0,
  },
}]]);
