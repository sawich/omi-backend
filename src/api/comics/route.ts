import { FastifyServer } from '../../shared/fastify';
import { ComicsController } from './controller';
import { homeRoutes } from './home/routes';
import { itemRoutes } from './item/routes';
import { searchRoutes } from './search/routes';

const comicsRoutes = async (fastify: FastifyServer, opts: any, next: any): Promise <void> => {
  const controller = new ComicsController();
  fastify.get('/comics/upload', controller.create);

  next();
};

export const routes = async (fastify: FastifyServer, opts: any, next: any) => {
  return Promise.all([
    comicsRoutes(fastify, opts, next),
    homeRoutes(fastify, opts, next),
    itemRoutes(fastify, opts, next),
    searchRoutes(fastify, opts, next),
  ]);
};
