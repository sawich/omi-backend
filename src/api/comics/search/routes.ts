
import { FastifyServer } from '../../../shared/fastify';

import { SearchController } from './controller';

export const searchRoutes = async (fastify: FastifyServer, opts: any, next: any): Promise <void> => {
  const controller = new SearchController();
  fastify.get('/comics/search', controller.search);

  next();
};
