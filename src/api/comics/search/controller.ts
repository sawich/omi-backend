import { FastifyRequest, FastifyReply } from 'fastify';

import { db, DBElement } from '../../../database';
import { IncomingMessage } from 'http';

interface ISearchRespone {
  id: number;

  // обложка
  cover: string;

  // название на языке пользователя
  name: string;
}

interface IQuery {
  // обязательные теги
  required?: number[];

  // игнорируемые теги
  ignored?: number[];

  // смещение относительно предыдущего ответа
  offset?: number;
}

type Request = FastifyRequest<IncomingMessage, IQuery>;

export class SearchController {
  public async search(req: Request): Promise <ISearchRespone[]> {
    return Array.from(db.values()).map((value: DBElement) => ({
      id: value.id,
      cover: value.cover,
      name: value.name,
    }));
  }
}
