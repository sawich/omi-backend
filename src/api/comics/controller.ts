import { FastifyHttpResponse } from '../../shared/fastify';
import { FastifyRequest, FastifyReply } from 'fastify';

import 'fastify-multipart';

export class ComicsController {
  public async create(
    req: FastifyRequest,
    reply: FastifyReply <FastifyHttpResponse>,
  ): Promise <void> {
    const files = req.multipart((
      field: string,
      file: any,
      filename: string,
      encoding: string,
      mimetype: string,
    ) => {
      console.log(filename);
    }, (err) => {
      console.error(err);
    });
  }
}
