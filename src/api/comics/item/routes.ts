
import { FastifyServer } from '../../../shared/fastify';

import { ItemController } from './controller';

export const itemRoutes = async (fastify: FastifyServer, opts: any, next: any): Promise <void> => {
  const controller = new ItemController();
  fastify.get('/comics/item/info', controller.info);
  fastify.get('/comics/item/comments', controller.comments);
  fastify.get('/comics/item/discussion_of_translation', controller.discussion_of_translation);

  next();
};
