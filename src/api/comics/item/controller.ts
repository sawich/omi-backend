import { FastifyRequest, FastifyReply } from 'fastify';
import { IncomingMessage } from 'http';
import { db, DBElement } from '../../../database';
import { NotFound } from 'http-errors';

interface IInfoRespone {
  id: number;
  cover: string;
  name: string;
  published: {
    start: number
    end: number,
  };
}

interface IDiscussionAuthorResponse {
  id: number;
  nick: string;
  name: string;
}

interface IDiscussionRespone {
  id: number;
  author: IDiscussionAuthorResponse;
  text: string;
  spoiler: boolean;
  likes: number;
  timestamp: number;
}

interface Query {
  id?: number;
}

type Request = FastifyRequest<IncomingMessage, Query>;

export class ItemController {
  public async info(req: Request): Promise <IInfoRespone> {
    const id = Number(req.query.id);
    if (isNaN(id)) { throw new NotFound(); }

    const item = db.get(id);
    if (undefined === item) { throw new NotFound(); }

    return {
      id: item.id,
      cover: item.cover,
      name: item.name,
      published: item.published,
    };
  }

  public async comments(req: Request): Promise <IDiscussionRespone[]> {
    const comments: IDiscussionRespone[] = [{
      id: 0,
      author: {
        id: 0,
        nick: 'sanadaeleven',
        name: '真田ジューイチ',
      },
      text: '宮迫はん帰ってこれないんだったらアメトーークはいっそ別の芸人さんに託したらいいと思う。同格くらいならココリコとか。',
      spoiler: false,
      likes: 0,
      timestamp: 1556060400,
    }, {
      id: 1,
      author: {
        id: 0,
        nick: 'sanadaeleven',
        name: '真田ジューイチ',
      },
      text: 'ガルパン最終章はいいぞ。',
      spoiler: true,
      likes: 0,
      timestamp: 1558306800,
    }];

    return comments;
  }

  public async discussion_of_translation(req: Request): Promise <IDiscussionRespone[]> {
    const comments: IDiscussionRespone[] = [{
      id: 0,
      author: {
        id: 0,
        nick: 'sanadaeleven',
        name: '真田ジューイチ',
      },
      text: '宮迫はん帰ってこれないんだったらアメトーークはいっそ別の芸人さんに託したらいいと思う。同格くらいならココリコとか。',
      spoiler: false,
      likes: 0,
      timestamp: 1556060400,
    }, {
      id: 1,
      author: {
        id: 0,
        nick: 'sanadaeleven',
        name: '真田ジューイチ',
      },
      text: 'ガルパン最終章はいいぞ。',
      spoiler: true,
      likes: 0,
      timestamp: 1558306800,
    }];

    return comments;
  }
}
