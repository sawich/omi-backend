
import { FastifyServer } from '../../../shared/fastify';

import { HomeController } from './controller';

export const homeRoutes = async (fastify: FastifyServer, opts: any, next: any): Promise <void> => {
  const controller = new HomeController();
  fastify.get('/comics/home/last', controller.last);
  fastify.get('/comics/home/recent', controller.recent);

  next();
};
