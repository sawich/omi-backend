import { FastifyRequest, FastifyReply } from 'fastify';

import { FastifyHttpResponse } from '../../../shared/fastify';
import { db, DBElement } from '../../../database';

interface ILastRespone {
  id: number;
  cover: string;
  name: string;
}

export class HomeController {
  public async last(
    req: FastifyRequest,
    reply: FastifyReply <FastifyHttpResponse>,
  ): Promise <ILastRespone[]> {
    return Array.from(db.values()).map((value: DBElement) => ({
      id: value.id,
      cover: value.cover,
      name: value.name,
    }));
  }

  public async recent(
    req: FastifyRequest,
    reply: FastifyReply <FastifyHttpResponse>,
  ): Promise <ILastRespone[]> {
    return Array.from(db.values()).map((value: DBElement) => ({
      id: value.id,
      cover: value.cover,
      name: value.name,
    }));
  }
}
