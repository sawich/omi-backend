
import { Guild, TextChannel, User, MessageReaction } from 'discord.js';

import { fastify, FastifyServer } from './shared/fastify';
import { routes as comicsRoutes } from './api/comics/route';

import { discord, channels } from './shared/discord';

const routes = async (f: FastifyServer): Promise <void> => {
  f.register(comicsRoutes);
};

interface DiscordPackage {
  op: number;
  d: {
    user_id: string;
    message_id: string;
    emoji: {
      name: string;
      id: number;
      animated: boolean;
    };
    channel_id: string;
    guild_id: string;
  };
  s: number;
  t: string;
}

class App {

  public constructor() {
    fastify.register(require('fastify-cors'));
    fastify.register(require('fastify-multipart'));

    const guild = discord.guilds.get('469205724824731648') as Guild;
    if (undefined === guild) {
      console.error('guild not found');
      process.exit(1);
    }

    channels.logs = guild.channels.get('600429279708315659') as TextChannel;
    if (undefined === channels.logs) {
      console.error('logs channel not found');
      process.exit(1);
    }

    discord.on('raw', this.rawMessageReactionAdd);

    discord.on('messageReactionAdd', this.messageReactionAdd);

    console.error(fastify.printRoutes());
  }
  public async main() {
    try {
      await fastify.listen(3000);
    } catch (err) {
      await channels.logs!.send(`${err}`);
      fastify.log.error(err);
    }
  }

  private rawMessageReactionAdd(packet: DiscordPackage) {
    if (![ 'MESSAGE_REACTION_ADD' ].includes(packet.t)) { return; }

    const channel = discord.channels.get(packet.d.channel_id) as TextChannel;

    if (
      channel.messages.has(packet.d.message_id) ||
      channels.wait !== channel
    ) { return; }

    channel.fetchMessage(packet.d.message_id).then((message) => {
      const emoji =
        packet.d.emoji.id
        ? `${packet.d.emoji.name}:${packet.d.emoji.id}`
        : packet.d.emoji.name;

      const reaction = message.reactions.get(emoji);

      if (reaction) {
        const user = discord.users.get(packet.d.user_id) as User;
        reaction.users.set(packet.d.user_id, user);
      }
      if (packet.t === 'MESSAGE_REACTION_ADD') {
        discord.emit('messageReactionAdd', reaction, discord.users.get(packet.d.user_id));
      }
    });
  }

  private messageReactionAdd(messageReaction: MessageReaction, user: User) {
    if (channels.wait !== messageReaction.message.channel) { return; }

    console.log('asd');
  }
}

const main = async () => {
  let app: App | undefined;

  try {
    await discord.login('NjAwMzc2MTMzMzMwMDEwMTQ1.XSy3zA.j7e0zP-Mlf28swuibXdYvW_E6zc');
    await routes(fastify);

    app = new App();
    await app.main();
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

main();
