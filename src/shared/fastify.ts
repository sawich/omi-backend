import fastifyFramework, { FastifyInstance } from 'fastify';

import { Server, IncomingMessage, ServerResponse } from 'http';

export type FastifyHttpResponse = ServerResponse;
export type FastifyServer = FastifyInstance<Server, IncomingMessage, FastifyHttpResponse>;

export const fastify: FastifyServer = fastifyFramework({ logger: true });
