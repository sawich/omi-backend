import { Client, TextChannel } from 'discord.js';

const discord = new Client();
discord.on('error', console.error);

export { discord };

interface IChannels {
  logs: TextChannel | undefined;
  allowed: TextChannel | undefined;
  refuse: TextChannel | undefined;
  wait: TextChannel | undefined;
}

export const channels: IChannels = {
  logs: undefined,
  allowed: undefined,
  refuse: undefined,
  wait: undefined,
};
